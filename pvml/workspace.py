from typing import Dict

from pvml import util, routes
from pvml.audit import AuditFilter
from pvml.bot import Bot
from pvml.connector.base import BaseConnector
from pvml.datasource import Datasource
from pvml.group import Group
from pvml.llm import LLM
from pvml.policy import Policy
from pvml.pvml_http_client import PvmlHttpClient
from pvml.view import View
from pvml.workspace_user import WorkspaceUser


class Workspace:
    """
    A Workspace object and client
    Attributes:
        id (str): The workspace id
        name (str): The name of the workspace
        description (str): The description of the workspace
    """

    def __init__(self, http_client: PvmlHttpClient, workspace_info):
        self.__http_client = http_client
        self.id = workspace_info['id']
        self.name = workspace_info['name']
        self.description = workspace_info['description']

    def __str__(self):
        return f"Workspace(workspace_name='{self.name}', workspace_id='{self.id})"

    def __repr__(self):
        return f"Workspace(workspace_name='{self.name}', workspace_id='{self.id})"

    @property
    def http_client(self):
        return self.__http_client

    def get_current_user(self) -> WorkspaceUser:
        """
        Fetch a WorkspaceUser client for the current api key
        :return: WorkspaceUser
        :raises Exception: If the API call fails
        """
        return self.get_user(self.http_client.user_id)

    def get_datasources(self) -> Dict[str, Datasource]:
        """
        Fetches all datasources connected to this workspace
        :return: A dictionary of Datasource clients mapped by their ids
        :raises Exception: If the API call fails.
        """
        url = routes.DATASOURCES.format(workspace_id=self.id)
        response_dict = self.http_client.request_async("GET", url)
        return {ds['id']: Datasource(self.http_client, ds) for ds in response_dict['datasources']}

    def get_users(self) -> Dict[str, WorkspaceUser]:
        """
        Fetches all users connected to this workspace
        :return: A dictionary of WorkspaceUser clients mapped by their ids
        :raises Exception: If the API call fails.
        """
        url = routes.USERS.format(workspace_id=self.id)
        response_dict = self.http_client.request_async("GET", url)
        return {wsu['User']['id']: WorkspaceUser(self.http_client, wsu) for wsu in response_dict['workspaceUsers']}

    def get_llms(self) -> Dict[str, LLM]:
        """
        Fetches all llms configured on this workspace
        :return: A dictionary of LLM clients mapped by their ids
        :raises Exception: If the API call fails.
        """
        url = routes.LLMS.format(workspace_id=self.id)
        response_dict = self.http_client.request_async("GET", url)
        return {llm['id']: LLM(self.http_client, llm) for llm in response_dict['workspaceLlms']}

    def get_bots(self) -> Dict[str, Bot]:
        """
        Fetches all bots configured on this workspace
        :return: A dictionary of Bot clients mapped by their ids
        :raises Exception: If the API call fails.
        """
        url = routes.BOTS.format(workspace_id=self.id)
        response_dict = self.http_client.request_async("GET", url)
        return {bot['id']: Bot(self.http_client, bot) for bot in response_dict['workspaceBots']}

    def get_bots_permitted(self) -> Dict[str, Bot]:
        """
        Fetches all bots that the current user is permitted to run
        :return: A dictionary of Bot clients mapped by their ids
        :raises Exception: If the API call fails.
        """
        url = routes.BOTS_PERMITTED.format(workspace_id=self.id)
        response_dict = self.http_client.request_async("GET", url)
        return {bot['id']: Bot(self.http_client, bot) for bot in response_dict['workspaceBots']}

    def get_views(self) -> Dict[str, View]:
        """
        Fetches all views configured on this workspace
        :return: A dictionary of View clients mapped by their ids
        :raises Exception: If the API call fails.
        """
        url = routes.VIEWS.format(workspace_id=self.id)
        response_dict = self.http_client.request_async("GET", url)
        return {view['viewId']: View(self.http_client, view) for view in response_dict['views']}

    def get_groups(self) -> Dict[str, Group]:
        """
        Fetches all groups configured on this workspace
        :return: A dictionary of Group clients mapped by their ids
        :raises Exception: If the API call fails.
        """
        url = routes.GROUPS.format(workspace_id=self.id) + '/?summary=true'
        response_dict = self.http_client.request_async("GET", url)
        return {group['id']: Group(self.http_client, group) for group in response_dict['groups']}

    def get_policies(self) -> Dict[str, Policy]:
        """
        Fetches all policies configured on this workspace
        :return: A dictionary of Policy clients mapped by their ids
        :raises Exception: If the API call fails.
        """
        url = routes.PERMISSIONS.format(workspace_id=self.id)
        response_dict = self.http_client.request_async("GET", url)
        return {permission['id']: Policy(self.http_client, permission) for permission in response_dict['permissions']}

    def create_llm(self, name: str, description: str, vendor_name: str, model_name: str,
                   token: str, props: dict) -> LLM:
        """
        Configures a new LLM in the workspace
        :param name: The name of the LLM
        :param description: The description of the LLM
        :param vendor_name: The name of the LLM vendor
        :param model_name: The name of the LLM model
        :param token: The token for the LLM
        :param props: The PVML configured properties for the LLM
        :return: An LLM client
        :raises Exception: If the API call fails
        """
        url = routes.LLMS.format(workspace_id=self.id)
        payload = {
            "name": name,
            "description": description,
            "workspaceId": self.id,
            "vendorName": vendor_name,
            "modelName": model_name,
            "token": token,
            "llmProps": props
        }
        response_dict = self.http_client.request_async("POST", url, json=payload)
        return LLM(self.http_client, response_dict)

    def get_llm(self, llm_id: str) -> LLM:
        """
        Fetches the details for a specific LLM
        :param llm_id: The ID of the LLM to retrieve details for
        :return: An LLM object containing the LLM details
        :raises Exception: If the API call fail
        """
        url = routes.LLM.format(workspace_id=self.id, llm_id=llm_id)
        response_dict = self.http_client.request_async("GET", url)
        return LLM(self.http_client, response_dict)

    def delete_llm(self, llm_id: str = None, llm: LLM = None):
        """
        Deletes a specific LLM
        :param llm_id: The ID of the LLM to delete
        :param llm: The LLM to delete
        :return: None
        :raises Exception: If the API call fails
        """
        llm_id = util.get_id(llm_id, llm)
        url = routes.LLM.format(workspace_id=self.id, llm_id=llm_id)
        self.http_client.request_async("DELETE", url)

    def create_adhoc_user(self, name: str) -> WorkspaceUser:
        """
        Creates a new adhoc user in the workspace
        :param name: The name of the adhoc user to create
        :return: WorkspaceUser client
        :raises Exception: If the API call fails
        """
        url = routes.USERS_ADHOC.format(workspace_id=self.id)
        payload = {'name': name}
        response_dict = self.http_client.request_async("POST", url, json=payload)
        return WorkspaceUser(self.http_client, response_dict)

    def get_user(self, user_id: str) -> WorkspaceUser:
        """
        Fetches a WorkspaceUser client for a specific user
        :param user_id: The ID of the user to retrieve
        :return: WorkspaceUser client
        :raises Exception: If the API call fails
        """
        url = routes.USER.format(workspace_id=self.id, user_id=user_id) + '?enrich=true'
        response_dict = self.http_client.request_async("GET", url)
        return WorkspaceUser(self.http_client, response_dict)

    def delete_user(self, user_id: str = None, user: WorkspaceUser = None) -> None:
        """
        Deletes a user from the project_workspace.
        :param user_id: The ID of the user to delete
        :param user: The WorkspaceUser to delete
        :raises Exception: If the API call fails
        """
        user_id = util.get_id(user_id, user)
        url = routes.USER.format(workspace_id=self.id, user_id=user_id)
        self.http_client.request_async("DELETE", url)

    def connect_datasource(self, connector: BaseConnector) -> Datasource:
        """
        Connects a new datasource to the project_workspace
        :param connector: The datasource configuration details
        :return: The connected Datasource client
        :raises Exception: If the API call fails
        """
        url = routes.DATASOURCES.format(workspace_id=self.id)
        response_dict = self.http_client.request_async("POST", url, json=connector.get_payload())
        return Datasource(self.http_client, response_dict)

    def get_datasource(self, datasource_id: str) -> Datasource:
        """
        Fetches a datasource client with the provided id
        :param datasource_id: The datasource id
        :return: A Datasource client that allows API calls
        :raises Exception: If the API call fails.
        """
        url = routes.DATASOURCE_DATA.format(workspace_id=self.id, datasource_id=datasource_id)
        response_dict = self.http_client.request_async("GET", url)
        return Datasource(self.http_client, response_dict)

    def delete_datasource(self, datasource_id: str = None, datasource: Datasource = None):
        """
        Deletes a datasource
        :param datasource_id: The ID of the datasource to delete
        :param datasource: The datasource to delete
        :return: None
        :raises Exception: If the API call fails
        """
        datasource_id = util.get_id(datasource_id, datasource)
        url = routes.DATASOURCE.format(workspace_id=self.id, datasource_id=datasource_id)
        self.http_client.request_async("DELETE", url)

    def create_group(self, name: str, description: str, image: bytes = None) -> Group:
        """
        Creates a new group
        :param name: The name of the group
        :param description: The description of the group
        :param image: The image of the group
        :return: The created Group client
        :raises Exception: If the API call fails
        """
        url = routes.GROUPS.format(workspace_id=self.id)
        request_dict = {'name': name, 'description': description}
        body_text, headers = self.http_client.metadata_request(request_dict, image)
        response_dict = self.http_client.request_async("POST", url, data=body_text, headers=headers)
        return Group(self.http_client, response_dict['group'])

    def get_group(self, group_id: str) -> Group:
        """
        Fetches the details of a specific group
        :param group_id: The ID of the group to retrieve
        :return: A Group client
        :raises Exception: If the API call fails
        """
        url = routes.GROUP.format(workspace_id=self.id, group_id=group_id) + '?enrich=true'
        response_dict = self.http_client.request_async("GET", url)
        return Group(self.http_client, response_dict['group'])

    def delete_group(self, group_id: str = None, group: Group = None) -> None:
        """
        Deletes the group
        :param group_id: The ID of the group to delete
        :param group: The group to delete
        :return: None
        :raises Exception: If the API call fails
        """
        group_id = util.get_id(group_id, group)
        url = routes.GROUP.format(workspace_id=self.id, group_id=group_id)
        self.http_client.request_async("DELETE", url)

    def create_view(self, name: str, description: str,
                    datasource_id: str = None, datasource: Datasource = None,
                    image: bytes = None) -> View:
        """
        Creates a new view for a specific datasource
        :param name: The name of the view
        :param description: A brief description of the view
        :param datasource_id: The datasource ID
        :param datasource: The datasource
        :param image: Binary data for the image to upload
        :return: A View client
        :raises Exception: If the API call fails
        """
        datasource_id = util.get_id(datasource_id, datasource)
        url = routes.VIEWS.format(workspace_id=self.id)
        request_dict = {'name': name, 'description': description, 'datasourceId': datasource_id}
        body_text, headers = self.http_client.metadata_request(request_dict, image)
        response_dict = self.http_client.request_async("POST", url, headers, data=body_text)
        return View(self.http_client, response_dict['view'])

    def get_view(self, view_id: str) -> View:
        """
        Fetches a view client with the provided id
        :param view_id: The ID of the view to retrieve
        :return: A View client
        :raises Exception: If the API call fails
        """
        url = routes.VIEW.format(workspace_id=self.id, view_id=view_id)
        response_dict = self.http_client.request_async("GET", url)
        return View(self.http_client, response_dict['view'])

    def delete_view(self, view_id: str = None, view: View = None) -> None:
        """
        Deletes a specific view
        :param view_id: The ID of the view to delete
        :param view: The view to delete
        :return: None
        :raises Exception: If the API call fails
        """
        view_id = util.get_id(view_id, view)
        url = routes.VIEW.format(workspace_id=self.id, view_id=view_id)
        self.http_client.request_async("DELETE", url)

    def create_bot(self, name: str, description: str,
                   instructions: str,
                   view_id: str = None, view: View = None,
                   llm_id: str = None, llm: LLM = None) -> Bot:
        """
        Creates a new bot with a specific llm
        :param name: The name of the view
        :param description: A brief description of the view
        :param instructions: The instructions for the bot
        :param llm_id: The llm ID for the bot
        :param llm: The llm for the bot
        :param view_id: A view_id to associate with the bot
        :param view: A view to associate with the bot
        :return: A Bot client
        :raises Exception: If the API call fails
        """
        ##
        llm_id = util.get_id(llm_id, llm)
        views = []
        if (view_id is not None) or (view is not None):
            views.append(util.get_id(view_id, view))
        url = routes.BOTS.format(workspace_id=self.id)
        payload = {
            "name": name,
            "description": description,
            "workspaceId": self.id,
            "llmId": llm_id,
            "instructions": instructions,
            "views": views,
            "createdBy": self.http_client.user_id,

        }
        response_dict = self.http_client.request_async("POST", url, json=payload)
        return Bot(self.http_client, response_dict)

    def get_bot(self, bot_id: str) -> Bot:
        """
        Fetches a bot client with the provided id
        :param bot_id: The ID of the bot to retrieve
        :return: A Bot object containing the bot details
        :raises Exception: If the API call fails
        """
        url = routes.BOT.format(workspace_id=self.id, bot_id=bot_id)
        response_dict = self.http_client.request_async("GET", url)
        return Bot(self.http_client, response_dict)

    def delete_bot(self, bot_id: str = None, bot: Bot = None):
        """
        Deletes a specific bot
        :param bot_id: The ID of the bot to delete
        :param bot: The bot to delete
        :return: None
        :raises Exception: If the API call fails
        """
        bot_id = util.get_id(bot_id, bot)
        url = routes.BOT.format(workspace_id=self.id, bot_id=bot_id)
        self.http_client.request_async("DELETE", url)

    def get_query_audit(
            self,
            page_size: int = 10,
            page_number: int = 1,
            override_projection_columns: list[str] = None,
            audit_filters: list[AuditFilter] = None,
    ) -> list:
        """
        Fetches query audit logs for the project_workspace.

        :param page_size: Number of records per page (default: 10)
        :param page_number: Page number to fetch (default: 1)
        :param override_projection_columns: List of columns to override
        :param audit_filters: List of filters to apply to the audit log
        :return: A list of audit logs
        :raises Exception: If the API call fails
        """
        default_projection_columns = ['id', 'query', 'duration', 'source', 'startTime', 'status', 'endTime',
                                      'userEmail',
                                      'userId', 'datasourceId', 'errorReason', 'traceId', 'errorType', 'datasourceType',
                                      'viewId', 'viewName', 'userQuestion', 'botId']
        projection_columns = (default_projection_columns
                              if (override_projection_columns is None)
                                 or (len(override_projection_columns) == 0)
                              else override_projection_columns)
        str_filter = ""
        if audit_filters is not None:
            post_filters = [
                "&{field_name}[{field_name}]={{operator:{operator},value:{value}}}".format(
                    field_name=iter_filter.field_name, operator=iter_filter.operator, value=iter_filter.value)
                for iter_filter in audit_filters]
            str_filter = "".join(post_filters)
        url = (routes.AUDIT.format(workspace_id=self.id) +
               f"?{','.join(projection_columns)}"
               f"&pageSize={page_size}&pageNumber={page_number}"
               f"{str_filter}")
        response_dict = self.http_client.request_async("GET", url)
        return response_dict['data']
