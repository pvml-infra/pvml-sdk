AVAILABLE_LLMS = '/llms'

WORKSPACES = '/workspaces'
WORKSPACE = '/workspaces/{workspace_id}'

USERS = WORKSPACE + '/users'
USERS_ADHOC = WORKSPACE + '/users/adhoc'

USER = WORKSPACE + '/users/{user_id}'
USER_GROUPS = USER + '/groups'
USER_WORKSPACES = '/users/{user_id}/workspaces'
USER_VIEWS = USER + '/views'
USER_TOKEN = USER + '/api_token'
USER_ROLE = USER + '/role'
USER_ATTRIBUTES = USER + '/attributes'

DATASOURCES = WORKSPACE + '/datasources'
DATASOURCE = WORKSPACE + '/datasources/{datasource_id}'
DATASOURCE_PING = DATASOURCE + '/ping'
DATASOURCE_DATA = DATASOURCE + '/data/secure'
DATASOURCE_TREE = DATASOURCE + '/tree'

DATASOURCE_RELATIONS = DATASOURCE + '/metadata/relations'
DATASOURCE_DESCRIPTIONS = DATASOURCE + '/metadata/descriptions'

DATASOURCE_PERMISSIONS = DATASOURCE + '/permissions'
DATASOURCE_PERMISSION = DATASOURCE + '/permissions/{permission_id}'

PERMISSIONS = WORKSPACE + '/permissions'

GROUPS = WORKSPACE + '/groups'
GROUP = WORKSPACE + '/groups/{group_id}'
GROUP_USERS = GROUP + '/users'

VIEWS = WORKSPACE + '/views'
VIEW = WORKSPACE + '/views/{view_id}'
VIEW_CONNECTION_STRING = VIEW + '/users/{user_id}/connection_strings'
VIEW_EXECUTE = VIEW + '/execute'
VIEW_ENTITIES = VIEW + '/entities'
VIEW_PERMISSIONS = VIEW + '/permissions'
VIEW_TREE_DISPLAY = VIEW + '/tree/display'

BOTS = WORKSPACE + '/bots'
BOT = WORKSPACE + '/bots/{bot_id}'
GENERATE = BOT + '/generate'
BOTS_PERMITTED = WORKSPACE + '/bots/permitted'

LLMS = WORKSPACE + '/llms'
LLM = WORKSPACE + '/llms/{llm_id}'

AUDIT = WORKSPACE + '/query-audit'
