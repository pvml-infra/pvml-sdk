from pvml import routes
from pvml.pvml_http_client import PvmlHttpClient
from pvml.util import convert_timestamp_to_datetime
from pvml.view import View


class Bot:
    """
    A bot object and client

    Attributes:
        workspace_id (str): The associated workspace id
        id (str): id of the bot
        llm_id (str): the id of the LLM
        name (str): name of the bot
        description (str): description of the bot
        instructions (str): the prompt instructions for the bot
        creation_time (int): The time when the bot was created (in milliseconds)
        created_by (str): the user that created the bot
    """

    def __init__(self, http_client: PvmlHttpClient, bot_info: dict):
        self.__http_client = http_client
        self.id = bot_info['id']
        self.workspace_id = bot_info['workspaceId']
        self.creation_time = convert_timestamp_to_datetime(bot_info['creationTime'])
        self.name = bot_info['name']
        self.view_id = bot_info['views'][0] if bot_info['views'] else None
        self.instructions = bot_info['instructions']
        self.description = bot_info['description']
        self.created_by = bot_info['createdBy']
        self.llm_id = bot_info['llmId']

    def __str__(self):
        return f"Bot(name='{self.name}', id='{self.id}')"

    def __repr__(self):
        return f"Bot(name='{self.name}', id='{self.id}')"

    @property
    def http_client(self):
        return self.__http_client

    def get_prompt(self) -> str:
        return self.instructions

    def get_llm_id(self) -> str:
        return self.llm_id

    def get_view(self) -> View | None:
        """
        Fetches a View client if available, otherwise returns None
        :return: A View object containing the view details
        :raises Exception: If the API call fails
        """
        if self.view_id is None:
            return None
        url = routes.VIEW.format(workspace_id=self.workspace_id, view_id=self.view_id)
        response_dict = self.http_client.request_async("GET", url)
        return View(self.http_client, response_dict['view'])

    def generate(self, input_dict) -> dict:
        """
        The method to call the LLM API
        :param input_dict: A dictionary containing the input data
        :return: The generated text
        :raises Exception: If the API call fails
        """
        if not isinstance(input_dict, dict):
            raise TypeError(f"Expected 'input' to be a dict, got {type(input_dict).__name__} instead.")
        url = routes.GENERATE.format(workspace_id=self.workspace_id, bot_id=self.id)
        return self.http_client.request_async('POST', url, json=input_dict)

    def invoke(self, input_dict: dict):
        return self.generate(input_dict)
