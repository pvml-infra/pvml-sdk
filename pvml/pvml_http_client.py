import asyncio
import json
import os
import uuid
from typing import Dict, Any

import aiohttp
import jwt
import nest_asyncio

nest_asyncio.apply()


class PvmlHttpClient:
    def __init__(self, api_key: str) -> None:
        if (api_key is None) or (api_key == ""):
            raise ValueError("api_key is required")
        self.__api_key = api_key
        decoded: Dict[str] = jwt.decode(api_key, options={"verify_signature": False})
        if (user_id := decoded.get("user", None)) is None:
            raise ValueError("user is required")
        self.__user_id = user_id
        self.__default_api_url = "https://platform.pvml.com/platform/api/v1"
        self.__session = aiohttp.ClientSession(loop=asyncio.get_event_loop())

    @property
    def api_key(self):
        return self.__api_key

    @property
    def user_id(self):
        return self.__user_id

    @property
    def session(self):
        return self.__session

    def __del__(self):
        if self.session is not None:
            try:
                asyncio.get_running_loop().create_task(self.session.close())
            except RuntimeError:
                print("Warning: No running event loop to close session in __del__")

    async def close(self):
        if self.session is not None:
            await self.session.close()
            self.__session = None

    def request_async(self, method, end_point, headers=None, **kwargs) -> Any:
        self.__method_validation(method)
        if headers is None:
            headers = self._get_auth_headers()
        return asyncio.run(self._request_async(method, end_point, headers, **kwargs))

    def __method_validation(self, method):
        allowed_methods = ["GET", "POST", "PUT", "PATCH", "DELETE"]
        if method not in allowed_methods:
            raise NotImplementedError(f"Method {method} is not supported")

    def _get_auth_headers(self) -> Dict[str, str]:
        """Constructs the authentication headers based on API key and secret."""
        return {"Content-Type": "application/json", "Authorization": f"Bearer {self.__api_key}"}

    async def _request_async(self, method, end_point, headers, **kwargs):
        api_base_url = os.environ.get("PVML_API_URL", self.__default_api_url)
        url = api_base_url + end_point
        async with self.__session.request(method, url, headers=headers, **kwargs) as response:
            if (response.status < 200) or (response.status >= 300):
                raise Exception(
                    f"HTTP {method.upper()} failed for URL {url} with status {response.status}, response: {await response.text()}"
                )
            data = await response.json()
            return data

    def metadata_request(self, request_dict, image):
        boundary = f"------WebKitFormBoundary_{uuid.uuid4()}"
        body = [f"--{boundary}", 'Content-Disposition: form-data; name="data"', "", json.dumps(request_dict)]

        if image is not None:
            body.append(f"--{boundary}")
            body.append('Content-Disposition: form-data; name="image"; filename="image.png"')
            body.append("Content-Type: image/png")
            body.append("")
            body.append(image.decode('latin1'))

        body.append(f"--{boundary}--")
        body.append("")
        body_text = "\r\n".join(body)

        headers = {
            "Content-Type": f"multipart/form-data; boundary={boundary}",
            "Authorization": f"Bearer {self.api_key}"
        }

        return body_text, headers
