from langchain_core.messages import HumanMessage

from examples.pvml_with_langchain.pvml_langchain_bot import PVMLExamplLangchainBot
from examples.pvml_with_langchain.utils import json_output_parser
from pvml import PVML

API_KEY = 'PVML_API_KEY'
pvml = PVML(API_KEY)

workspace = pvml.get_workspace('WORKSPACE_ID')
db_rag_bot = PVMLExamplLangchainBot(workspace.get_bot('RAG_BOT_ID'))
analysis_bot = workspace.get_bot('INSIGHTS_BOT_ID')

sequence = db_rag_bot | json_output_parser | analysis_bot.invoke
query = HumanMessage(content="Show the top 5 address cities by transaction volume for March and also for January.")
ans = sequence.invoke([query])
print(ans)
