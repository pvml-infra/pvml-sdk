import json
from typing import Any, Callable, Dict, Iterator, List, Optional, Sequence, Union

from langchain_core.callbacks import CallbackManagerForLLMRun
from langchain_core.language_models import BaseChatModel, LanguageModelInput
from langchain_core.messages import BaseMessage, AIMessage, ChatMessage
from langchain_core.outputs import ChatGenerationChunk, ChatResult, ChatGeneration
from langchain_core.runnables import Runnable
from langchain_core.tools import BaseTool

from examples.pvml_with_langchain.utils import message_to_json
from pvml.bot import Bot


class PVMLExamplLangchainBot(BaseChatModel, Bot):

    def __init__(self, bot: Bot):
        super().__init__()
        self.bot = bot

    def __str__(self):
        return f"PVMLExampleBot(llm_name='{self.bot.name}', llm_id='{self.bot.id})"

    def __repr__(self):
        return f"PVMLExampleBot(llm_name='{self.bot.name}', llm_id='{self.bot.id})"

    def _call_llm_api(self, messages: List[BaseMessage], stop: Optional[List[str]] = None) -> str:
        """
        Internal method to call the LLM API with the new endpoint and payload structure.

        :param messages: The input prompt for the LLM.
        :param stop: Optional list of stop sequences.
        :return: The generated text.
        """
        messages = {'messages': [message_to_json(msg) for msg in messages]}
        return self.bot.invoke(messages)

    def bind_tools(
            self,
            tools: Sequence[
                Union[Dict[str, Any], type, Callable, BaseTool]
            ],
            **kwargs: Any
    ) -> Runnable[LanguageModelInput, BaseMessage]:
        """
        Bind tools to the LLM.

        :param tools: A sequence of tools to bind, which can be dictionaries, types, callables, or instances of `BaseTool`.
        :param kwargs: Additional arguments for tool binding.
        :return: A runnable object that integrates the LLM with the bound tools.
        """

        def runnable(input: LanguageModelInput) -> BaseMessage:
            tool_response = f"Tool-bound response for input: {input}"
            return AIMessage(content=tool_response)

        return Runnable(runnable)

    def _stream(
            self,
            messages: List[BaseMessage],
            stop: Optional[List[str]] = None,
            run_manager: Optional[CallbackManagerForLLMRun] = None,
            **kwargs: Any
    ) -> Iterator[ChatGenerationChunk]:
        """
        Stream the results from the LLM.

        :param messages: List of input messages.
        :param stop: Optional stop sequences.
        :param run_manager: Optional callback manager for run tracking.
        :return: Iterator over ChatGenerationChunk objects.
        """
        streamed_data = self._call_llm_api(messages, stop)
        yield ChatGenerationChunk(content=streamed_data)

    def _generate(
            self,
            messages: List[BaseMessage],
            stop: Optional[List[str]] = None,
            run_manager: Optional[CallbackManagerForLLMRun] = None,
            **kwargs: Any
    ) -> ChatResult:
        """
        Generate a response based on the input messages.

        :param messages: List of input messages.
        :param stop: Optional stop sequences.
        :param run_manager: Optional callback manager for run tracking.
        :return: A ChatResult object containing the generated response.
        """
        generated_text = self._call_llm_api(messages, stop)
        if isinstance(generated_text, dict):
            generated_text = json.dumps(generated_text)
        chat_message = ChatMessage(role="ai", content=generated_text)
        chat_generation = ChatGeneration(message=chat_message)
        return ChatResult(generations=[chat_generation])

    @property
    def _llm_type(self) -> str:
        """
        Return a unique identifier for the LLM type.
        """
        return "PVML"
