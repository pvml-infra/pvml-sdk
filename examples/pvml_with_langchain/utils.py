import json

from langchain_core.messages import BaseMessage, HumanMessage, AIMessage, SystemMessage, ChatMessage


def json_output_parser(message: BaseMessage):
    return json.loads(message.content)


def message_to_json(message: BaseMessage):
    return {
        "type": type(message).__name__,
        "content": message.content,
        "metadata": message.additional_kwargs
    }


def dict_to_message(data: dict) -> BaseMessage:
    """
        Convert a dictionary into a BaseMessage (or one of its subclasses).

        The dictionary should have:
        - "type": A string indicating the message type ("human", "ai", "system", or another role).
        - "content": The textual content of the message.
        - Additional keys can be included as extra attributes in `additional_kwargs`.
        """
    msg_type = data.pop("type", None)
    content = data.pop("content", "")
    additional_kwargs = data  # All other fields become additional_kwargs

    if msg_type == "human":
        return HumanMessage(content=content, additional_kwargs=additional_kwargs)
    elif msg_type == "ai":
        return AIMessage(content=content, additional_kwargs=additional_kwargs)
    elif msg_type == "system":
        return SystemMessage(content=content, additional_kwargs=additional_kwargs)
    else:
        # If the message type isn't recognized, default to a generic ChatMessage with the given role.
        # If no msg_type was found, use "generic" as the role.
        role = msg_type if msg_type is not None else "generic"
        return ChatMessage(role=role, content=content, additional_kwargs=additional_kwargs)
