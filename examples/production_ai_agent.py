from datetime import datetime
from typing import Dict

from pysondb import db

from pvml import PVML
from pvml.util import get_epoch

SYSTEM_API_KEY = 'ADMIN_API_KEY'
project_workspace_id = 'PROJECT_WORKSPACE_ID'

pvml = PVML(SYSTEM_API_KEY)
project_workspace = pvml.get_workspace(project_workspace_id)
users_database = db.getDb("users_database.json")
project_group = project_workspace.get_group('PROJECT_GROUP_ID')


def get_user_from_db(user_id) -> Dict:
    user = users_database.getByQuery({"user_id": user_id})
    if user:
        user = user[0]
    return user


def add_get_user_db(user_id, pvml_user_id, last_token, expiration_date):
    users_database.add({
        "user_id": user_id,
        "pvml_user_id": pvml_user_id,
        "pvml_token": last_token,
        "expiration_date": expiration_date
    })
    return get_user_from_db(user_id)


def update_user_token(user_id, new_token, expiration_date):
    return users_database.updateById(user_id, {'pvml_token': new_token,
                                               'expiration_date': expiration_date})


def create_token(pvml_user, days=1):
    expiration_date = get_epoch(days)
    user_token = pvml_user.create_token(expiration_date)
    return user_token, expiration_date


def get_user_token(user_id):
    db_user = get_user_from_db(user_id)

    if not db_user:
        pvml_user = project_workspace.create_adhoc_user(name=user_id)
        user_token, expiration_date = create_token(pvml_user)
        db_user = add_get_user_db(user_id, pvml_user.id, user_token, expiration_date)
        # add user to group
        project_group.update_users([pvml_user.id])

    pvml_user = project_workspace.get_user(db_user['pvml_user_id'])
    time_now = int(datetime.now().timestamp()) * 1000
    if time_now < db_user["expiration_date"]:
        return db_user["pvml_token"]

    new_token, expiration_date = create_token(pvml_user)
    update_user_token(user_id, new_token, expiration_date)
    return new_token


def run(user_id, query):
    pvml_user_token = get_user_token(user_id)
    user_workspace = PVML(pvml_user_token).get_workspace(project_workspace_id)

    bot = user_workspace.get_bots_permitted()['BOT_ID']
    insights_bot = user_workspace.get_bots_permitted()['INSIGHTS_BOT_ID']

    rag_result = bot.invoke({'user_query': query})
    return insights_bot.invoke(rag_result)


ans = run('12345', "Show the top 5 address cities by transaction volume for March and also for January.")
print(ans)
